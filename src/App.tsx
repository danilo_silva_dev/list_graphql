import React, { useEffect, useState } from 'react';
import './App.css';
import axios from 'axios'
import { BASE_URL, GET_REPOSITORIES} from './constants'

type repo = {
  name: string;
  forks: number;
  stars: number;
}

function App() {
  const [repositories, setRepositories] = useState<repo[]>([])

  const fetchData = async () => {
    const queryResult = await axios.post(
      BASE_URL,
      {
        query: GET_REPOSITORIES
      }
    )
    //setRepositories(queryResult.data);
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <>
      <ul>
        {repositories.map(data => (
        <li>
          {data.name} - 🌟 {data.stars} - 🍴 {data.forks}
        </li>
      ))}
      </ul>
    </>
  );
}

export default App;
